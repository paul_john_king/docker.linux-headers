# © 2019 Paul John King (paul_john_king@web.de).  All rights reserved.
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License, version 3 as published by the
# Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

ARG PROJECT_NAME
ARG PROJECT_VERSION
ARG PROJECT_URL

ARG WORKBENCH_IMAGE="registry.gitlab.com/paul_john_king/docker.ubuntu_workbench:0.0.18_7efec42"

ARG LINUX_REPO="git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git"
ARG LINUX_VERSION="5.2"

ARG WORK_DIR="/work"
ARG SOURCE_DIR="${WORK_DIR}/sources"
ARG TARGET_DIR="${WORK_DIR}/targets"

FROM "${WORKBENCH_IMAGE}" AS workbench

	ARG LINUX_REPO
	ARG LINUX_VERSION

	ARG WORK_DIR
	ARG SOURCE_DIR
	ARG TARGET_DIR

	RUN \
		set -e; \
		set -u; \
		mkdir -p \
			"${SOURCE_DIR}" \
			"${TARGET_DIR}"; \
		git clone \
			--depth "1" \
			--branch "v${LINUX_VERSION}" \
			"${LINUX_REPO}" "${SOURCE_DIR}"; \
		cd "${SOURCE_DIR}"; \
		make headers_install INSTALL_HDR_PATH="${TARGET_DIR}"; \
	return;

FROM "scratch"

	ARG PROJECT_NAME
	ARG PROJECT_VERSION
	ARG PROJECT_URL

	ARG WORKBENCH_IMAGE

	ARG LINUX_REPO
	ARG LINUX_IMAGE

	ARG TARGET_DIR

	LABEL \
		project.name="${PROJECT_NAME}" \
		project.version="${PROJECT_VERSION}" \
		project.url="${PROJECT_URL}" \
		workbench.image="${WORKBENCH_IMAGE}" \
		linux.repo="${LINUX_REPO}" \
		linux.version="${LINUX_VERSION}"

	COPY --from="workbench" "${TARGET_DIR}" "/"
